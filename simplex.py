import numpy as np
import sys
from math import inf
from time import time


def main():
    filename = ' '.join(sys.argv[1:])

    npz_file = None
    try:
        npz_file = np.load(filename)
    except FileNotFoundError:
        print('File ' + filename + ' could not be found')
        exit()

    c = npz_file['c'].astype(float) * (-1)
    b = npz_file['b'].astype(float)
    A = npz_file['A'].astype(float)
    bix = npz_file['bix'] - 1
    z_cheat = npz_file['zcheat']
    x_cheat = npz_file['xcheat']

    print('c:\n' + str(c))
    print('b:\n' + str(b))
    print('A:\n' + str(A))
    print('bix:\n' + str(bix))
    print('z_cheat:\n' + str(z_cheat))
    print('x_cheat:\n' + str(x_cheat))

    [rows, cols] = np.shape(A)
    # print('Rows: ' + str(rows) + '\n' + 'Cols: ' + str(cols))

    nix = np.setdiff1d(range(cols), bix)
    print('nix:\n' + str(nix))

    B = A[:, bix]
    N = A[:, nix]
    cB = c[bix]
    cN = c[nix]

    '''A = N.transpose()
    A = np.c_[A, np.eye(cN.size)]
    [rows, cols] = np.shape(A)

    c = np.pad(b, (0, cN.size), 'constant') * (-1)
    b = cN * (-1)
    print(c)
    print(b)
    print(A)'''

    z = 0

    print('B:\n' + str(B))
    print('N:\n' + str(N))
    print('cB:\n' + str(cB))
    print('cN:\n' + str(cN))
    print('\n\n\n\n')

    start_time = time()
    iterates = 0
    done = False

    while not done:
        iterates += 1
        print(c)
        # välj inkommande variabel (välj det högsta värdet i c)
        # välj utgående variabel (välj det lägsta positiva värde)
        # formatera matrisen så att inkommande variabels kolumn bara är nollor och en etta där inkommande och utgående möts
        # byt ut utgående mot inkommande i bix
        # repetera tills endast positiva värden fås i c

        piv_col = get_pivot_column(c, cols)
        piv_row = get_pivot_row(A, b, rows, piv_col)
        if piv_row == -1:
            done = True
            z = inf

        else:
            A, b, c, z = format_matrix(A, b, c, z, piv_row, piv_col, rows)
            bix = change_base(bix, piv_row, piv_col)

            done = only_negative(c, cols)

    run_time = time() - start_time
    print('Iterations: ' + str(iterates))
    print('z: ' + str(z))
    print('z_cheat: ' + str(z_cheat))
    x_values = []
    for x in range(cols):
        if x in bix:
            x_values.append(b[np.where(bix == x)][0])
        else:
            x_values.append(0)

    print('x: ' + str(x_values))
    print('x_cheat: ' + str(x_cheat))

    print('Time: ' + str(run_time))


def get_pivot_column(c, cols):
    max_value = -inf
    index = 0
    for col in range(cols):
        if c[col] > max_value and c[col] != 0:
            max_value = c[col]
            index = col
    return index


def get_pivot_row(A, b, rows, piv_col):
    min_value = inf
    index = -1
    for row in range(rows):
        try:
            value = b[row] / A[row, piv_col]
            if 0 < value < min_value:
                min_value = value
                index = row
        except ZeroDivisionError:
            pass
    return index


def format_matrix(A, b, c, z, piv_row, piv_col, rows):
    piv_item = A[piv_row, piv_col]
    A[piv_row] /= piv_item
    b[piv_row] /= piv_item

    mult = -(c[piv_col])
    c = c + (A[piv_row] * mult)
    z = z + (b[piv_row] * mult)

    for row in range(rows):
        if row != piv_row:
            mult = -(A[row, piv_col])
            A[row] = A[row] + (A[piv_row] * mult)
            b[row] = b[row] + (b[piv_row] * mult)

    return A, b, c, z


def change_base(bix, piv_row, piv_col):
    bix[piv_row] = piv_col
    return bix


def only_negative(c, cols):
    for col in range(cols):
        if c[col] > 0:
            return False
    return True


if __name__ == '__main__':
    main()
