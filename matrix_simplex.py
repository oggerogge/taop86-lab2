import numpy as np
import sys
from math import inf
from time import time
from copy import copy


def main():
    filename = ' '.join(sys.argv[1:])

    npz_file = None
    try:
        npz_file = np.load(filename)
    except FileNotFoundError:
        print('File ' + filename + ' could not be found')
        exit()

    c = npz_file['c'].astype(float)
    b = npz_file['b'].astype(float)
    A = npz_file['A'].astype(float)
    bix = npz_file['bix'] - 1
    z_cheat = npz_file['zcheat']
    x_cheat = npz_file['xcheat']

    print('c:\n' + str(c))
    print('b:\n' + str(b))
    print('A:\n' + str(A))
    print('bix:\n' + str(bix))
    print('z_cheat:\n' + str(z_cheat))
    print('x_cheat:\n' + str(x_cheat))

    [rows, cols] = np.shape(A)
    # print('Rows: ' + str(rows) + '\n' + 'Cols: ' + str(cols))

    nix = np.setdiff1d(range(cols), bix)
    print('nix:\n' + str(nix))

    B = A[:, bix]
    N = A[:, nix]
    cB = c[bix]
    cN = c[nix]
    xB = None
    z = 0

    print('B:\n' + str(B))
    print('N:\n' + str(N))
    print('cB:\n' + str(cB))
    print('cN:\n' + str(cN))
    print('\n\n\n\n')

    start_time = time()
    iterates = 0
    done = False
    piv_row = 0
    piv_col = 0

    while not done:
        iterates += 1
        # print(B)
        B_inv = np.linalg.inv(B)
        xB = np.dot(B_inv, b)
        # print(xB)
        # nix = 0
        y = np.transpose(np.dot(np.transpose(cB), B_inv))
        z = np.dot(np.transpose(b), y)
        '''
        print('N:\n' + str(N))
        print('B:\n' + str(B))
        print('cN:\n' + str(cN))
        print('cB:\n' + str(cB))
        print('bix:\n' + str(bix))
        print('nix:\n' + str(nix))
        print('xB:\n' + str(xB))
        '''
        cN_hatt = cN - np.dot(np.transpose(N), y)
        '''
        print('cN_hatt:\n' + str(cN_hatt))
        print('z:\n' + str(z))
        print('\n\n\n\n\n')
        '''

        if (cN_hatt >= -0.0000001).all():
            done = True
            # print(z)
        else:
            piv_col = get_pivot_column(cN_hatt, cN.size)
            ak_hat = np.dot(B_inv, N[:, piv_col])
            # print(ak_hat)
            if (ak_hat <= 0).all():
                z = inf
                done = True
            else:
                piv_row = get_pivot_row(ak_hat, xB, xB.size)

                bix, nix = change_base(bix, nix, piv_row, piv_col)

                temp = copy(B[:, piv_row])
                B[:, piv_row] = N[:, piv_col]
                N[:, piv_col] = temp

                temp = copy(cN[piv_col])
                cN[piv_col] = cB[piv_row]
                cB[piv_row] = temp

    end_time = time()
    print('Iterations: ' + str(iterates))
    print('z: ' + str(z))
    print('z_cheat: ' + str(z_cheat))
    x_values = []
    for x in range(cols):
        if x in bix:
            x_values.append(xB[np.where(bix == x)][0])
        else:
            x_values.append(0)
    print('x: ' + str(x_values))
    print('x_cheat: ' + str(x_cheat))

    print('Time: ' + str(end_time - start_time))


def get_pivot_column(c, cols):
    min_value = inf
    index = 0
    for col in range(cols):
        if c[col] < min_value and c[col] != 0:
            min_value = c[col]
            index = col
    return index


def get_pivot_row(aK_hat, xB, xB_size):
    min_value = inf
    index = 0
    for row in range(xB_size):
        try:
            if xB[row] > 0:
                value = xB[row] / aK_hat[row]
                if 0 < value < min_value:
                    min_value = value
                    index = row
        except ZeroDivisionError:
            pass
    return index


def change_base(bix, nix, piv_row, piv_col):
    temp1 = copy(nix[piv_col])
    temp2 = copy(bix[piv_row])
    nix[piv_col] = temp2
    bix[piv_row] = temp1

    return bix, nix


if __name__ == '__main__':
    main()
